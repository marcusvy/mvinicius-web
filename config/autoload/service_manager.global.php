<?php
/**
 * Created by PhpStorm.
 * User: Marcus
 * Date: 11/06/2015
 * Time: 13:25
 */
return array(
  'service_manager' => array(
    'abstract_factories' => array(
      'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
      'Zend\Log\LoggerAbstractServiceFactory',
    ),
  ),
);
