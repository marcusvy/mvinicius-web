<?php
/**
 * Created by PhpStorm.
 * User: Marcus
 * Date: 11/06/2015
 * Time: 13:23
 */
return array(
  'service_manager' => array(
    'factories' => array(
      'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
    ),
  ),
  'translator' => array(
    'locale' => 'pt_BR',
    'translation_file_patterns' => array(
      array(
        'type' => 'gettext',
        'base_dir' => __DIR__ . '../../../resources/i18n',
        'pattern' => '%s.mo',
      ),
    ),
  ),
);
