<?php
return array(
  'caches' => array(
    'Cache\Transient' => [
      'adapter' => 'redis',
      'ttl'     => 60,
      'plugins' => [
        'exception_handler' => [
          'throw_exceptions' => false,
        ],
      ],
    ],
    'Cache\Persistence' => [
      'adapter' => 'filesystem',
      'ttl'     => 86400,
    ],
  ),
);
