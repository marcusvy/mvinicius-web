<?php
/**
 * Created by PhpStorm.
 * User: Marcus
 * Date: 11/06/2015
 * Time: 13:24
 */
return array(

  // Initial configuration with which to seed the ServiceManager.
  // Should be compatible with Zend\ServiceManager\Config.
  // 'service_manager' => array(),
  // ViewManager configuration
  'view_manager' => array(

    // Doctype with which to seed the Doctype helper
    'doctype' => 'HTML5', // e.g. HTML5, XHTML1

    // TemplateMapResolver configuration
    // template/path pairs
    'template_map' => array(

    ),

    // TemplatePathStack configuration
    // module/view script path pairs
    'template_path_stack' => array(),
    // Default suffix to use when resolving template scripts, if none, 'phtml' is used
    'default_template_suffix' => 'phtml', // e.g. 'php'

    // Layout template name
    'layout' => 'layout/layout', // e.g. 'layout/layout'

    // ExceptionStrategy configuration
    'display_exceptions' => true, // display exceptions in template
    'exception_template' => 'error/index', // e.g. 'error'

    // RouteNotFoundStrategy configuration
    'display_not_found_reason' => true, // display 404 reason in template
    'not_found_template' => 'error/404', // e.g. '404'

    // Additional strategies to attach
    // These should be class names or service names of View strategy classes
    // that act as ListenerAggregates. They will be attached at priority 100,
    // in the order registered.
    'strategies' => array(
      'ViewJsonStrategy', // register JSON renderer strategy
      'ViewFeedStrategy', // register Feed renderer strategy
    ),
  ),
);
