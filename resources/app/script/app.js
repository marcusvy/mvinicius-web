(function () {
  'use strict';
  angular.module('MyApp', ['ngMaterial'])
    .config(ThemeProvider);

  function ThemeProvider($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('light-green')
      .accentPalette('amber')
      .warnPalette('red');
  }
})();
