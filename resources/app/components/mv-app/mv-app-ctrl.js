(function () {
  'use strict';

  class MvAppCtrl{

    constructor($mdSidenav){
      this.$inject = ['$mdSidenav'];
      this.$mdSidenav = $mdSidenav;
    }

    openLeftMenu () {
      this.$mdSidenav('left').toggle();
    }
  }

  angular
    .module('MyApp')
    .controller('MvAppCtrl', MvAppCtrl);

})();
