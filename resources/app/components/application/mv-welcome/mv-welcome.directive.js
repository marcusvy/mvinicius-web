angular
  .module('MyApp')
  .directive('mvWelcome', mvWelcomeDirective);

function mvWelcomeDirective() {
  return {
    strict: 'EA',
    //templateUrl: 'app/components/mv-welcome/mv-welcome.html',
    templateUrl: '/app-ng/welcome',
    controller: 'mvWelcomeCtrl',
    controllerAs: 'ctrl'
  };
}
