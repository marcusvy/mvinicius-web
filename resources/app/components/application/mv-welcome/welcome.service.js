(function () {
  'use strict';

  angular
    .module('MyApp')
    .factory('WelcomeService', WelcomeService);

  function WelcomeService($http) {
    var Service = {
      message: {},
      getMessage: getMessage,
      setMessage: setMessage
    };

    return Service;

    function getMessage() {
      var getMessageComplete = (response) => {return response.data};

      var getMessageError = (error) => {
        var msg = {
          title: 'Ops!',
          message: 'Servidor não operando corretamente... :('
        };
        Service.setMessage(msg);
        return Service.message;
      }

      return $http.get('/app/index/welcome')
        .then(getMessageComplete, getMessageError);
    }

    function setMessage(msg) {
      Service.message = msg;
      return Service;
    }
  }

  WelcomeService.$inject = ['$http'];

})();

