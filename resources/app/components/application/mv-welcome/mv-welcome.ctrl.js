(function () {
  'use strict';

  angular
    .module('MyApp')
    .controller('mvWelcomeCtrl', mvWelcomeCtrl);

  function mvWelcomeCtrl($scope, WelcomeService) {
    var vm = this;

    WelcomeService.getMessage()
      .then(function (msg) {
        vm.title = msg.title;
        vm.message = msg.message;
      });

    vm.horario = "";

    $scope.$watch('ctrl.horario', function (current, old) {
      console.log(current);
    });
  }

  mvWelcomeCtrl.$inject = ['$scope', 'WelcomeService'];

})();
