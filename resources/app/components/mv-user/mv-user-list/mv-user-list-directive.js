(function () {
  "use strict";

  class MvUserListDirective {

    constructor() {
      this.strict = 'E';
      this.templateUrl = '/mv/user-ng/user-list';
      this.controller = 'MvUserListCtrl';
      this.controllerAs = 'vm';
    }
  }

  angular
    .module("MyApp")
    .directive('mvUserList', () => new MvUserListDirective());

  //export default MvUserListDirective;

})();
