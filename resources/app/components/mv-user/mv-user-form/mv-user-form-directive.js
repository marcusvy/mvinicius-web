(function () {
  "use strict";

  class MvUserFormDirective {

    constructor() {
      this.strict = 'E';
      this.templateUrl = '/mv/user-ng/user-form';
      this.controller = 'MvUserFormCtrl';
      this.controllerAs = 'vm';
    }
  }

  angular
    .module("MyApp")
    .directive('mvUserForm', () => new MvUserFormDirective());

  //export default MvUserFormDirective;

})();
