(function () {
  "use strict";

  class MvUserFormCtrl {
    constructor(UserService) {
      this.$inject = ['UserService'];
      this._list = [];
      this.UserService = UserService;
      this.loadUsers();
      this.user = {};
    }

    get list() {
      return this._list;
    }

    loadUsers() {
      this.UserService.getUsers()
        .then(response => {
          this._list = response.collection;
          console.log(this._list);
        })
    }

  }

  angular.module('MyApp')
    .controller('MvUserFormCtrl', MvUserFormCtrl);

  //export default MvUserListCtrl;
})();
