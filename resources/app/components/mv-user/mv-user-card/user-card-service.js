(function(){
  "use strict";

class MvUserCardService {
  constructor() {
    this._list = [
      'Nome 1',
      'Nome 2',
      'Nome 3'
    ];
  }

  get list(){
    return this._list;
  }

}

angular
  .module('MyApp')
  .factory('MvUserCardService', () => new MvUserCardService());
})();
