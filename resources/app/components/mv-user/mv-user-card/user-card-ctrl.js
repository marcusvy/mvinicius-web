(function () {
  "use strict";

  class MvUserCardCtrl {

    constructor(MvUserCardService) {
      this.$inject = ['MvUserCardService'];
      this._name = 'Ragnarog';
      this._list = MvUserCardService.list;
    }

    get name() {
      return this._name;
    }

    get list(){
      return this._list;
    }

    avisa(number){
      console.log('teclando ',number);
    }
  }

  angular
    .module('MyApp')
    .controller('MvUserCardCtrl', MvUserCardCtrl);

})();
