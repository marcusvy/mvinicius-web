(function () {
  "use strict";

  class MvUserCardDirective {

    constructor() {
      this.strict = 'EA';
      this.template = [
        '<md-button class="md-raised" ng-repeat="user in vm.list" ng-click="vm.avisa(user)">',
        '{{user}}',
        '</md-button>',
        '<div ng-transclude></div>'
      ].join('');
      this.transclude = true;
      this.controller = 'MvUserCardCtrl';
      this.controllerAs = 'vm';
    }

  }

  angular
    .module('MyApp')
    .directive('mvUserCard', () => new MvUserCardDirective());
})();
