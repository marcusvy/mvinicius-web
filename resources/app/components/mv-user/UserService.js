(function () {
  "use strict";

  class UserService {
    constructor($http) {
      this.$inject = ['$http'];
      this._list = [];
      this.$http = $http;
    }

    get list() {
      return this._list;
    }

    getUsers() {
      return this.$http.get('/mv/user/user/list')
        .then(this.ajaxSuccess, this.ajaxError);
    }

    ajaxSuccess(response){
      return response.data;
    }

    ajaxError(error){
      let message = {
        message: "Connection Error",
        error: true
      };
      return message
    }
  }

  angular.module("MyApp")
    .factory('UserService', [
      '$http',
      ($http) => new UserService($http)
    ]);
})();
