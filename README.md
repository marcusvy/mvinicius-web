MVinicius Project
=======================

Introdu��o
------------
This is a simple, skeleton application using the ZF2 MVC layer and module
systems. This application is meant to be used as a starting place for those
looking to get their feet wet with ZF2.

Installation using Composer
---------------------------



## Author
![MVinicius](/icon/icon-mv.png)

**Marcus Vin�cius R G Cardoso**

(CEO & Fundador da MVinicius Consultoria)


- E-mail: <mailto:marcus@mviniciusconsultoria.com.br>
- Site: <http://mviniciusconsultoria.com.br>

## Copyright

2013-2014 MVinicius Consultoria, by
[GNU General Public License (GPLv3)](http://www.gnu.org/licenses/gpl-3.0.txt).
Documentation under GPL Licence
