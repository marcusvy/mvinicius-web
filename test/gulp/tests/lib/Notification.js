var assert = require('assert');
var Provider = {};
Notification = require('./../../lib/Notification');

describe("Lib: Notification", function() {
  it("Analisando retorno do objeto", function(done) {
    assert(typeof(Notification) == 'object');
    done();
  });

  it("Verificando métodos", function(done) {
    var msg = "Não possui método: ";
    var objProto = Object.getPrototypeOf(Notification);
    var methods = [
      'log',
      'error',
      'fileError',
      'forPassedTests',
      'forFailedTests',
      'message'
    ];
    methods.forEach(function(value, index) {
      assert.ok(objProto.hasOwnProperty(value), msg + value);
    });
    done();
  });
});
