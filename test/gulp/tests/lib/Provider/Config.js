var assert = require('assert');
var Provider = {};
Provider.Config = require('./../../../lib/Provider/Config');

describe("Lib: Config Provider", function() {
  it("Analisando retorno do objeto", function(done) {
    assert(typeof(Provider.Config) == 'object');
    done();
  });

  //it("Retornando config padrão", function(done) {
  //  var defaultSettings = { file: 'the-old-man.json', config: {} };
  //  assert.deepEqual(defaultSettings, Provider.Config);
  //  done();
  //});

  it("Analisando carregamento",function (done) {
    var defaultSettings = Provider.Config;
    var addSettings = {production:false}
    var newSettings = Provider.Config.load(addSettings);
    assert.equal(Provider.Config.config,newSettings);
    done();
  });

});
