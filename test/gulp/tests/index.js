var assert = require('assert');
var _ = require('underscore');
var Provider = {};
Tom = require('./../index.js');

describe("The Old Man: Main Class", function () {
  it("Analisando retorno do objeto", function (done) {
    assert(typeof(Tom) == 'object');
    done();
  });

  it("Verificando métodos", function (done) {
    var msg = "Não possui método: ";
    var objProto = Object.getPrototypeOf(Tom);
    var methods = [
      "onError",
      "onSize",
      "registerTasks",
      "do",
      "start",
      "task"
    ];
    methods.forEach(function (value, index) {
      assert.ok(objProto.hasOwnProperty(value), msg + value);
    });
    done();
  });
});


describe("Habilitação de Tarefas", function () {
  beforeEach(function(){
    Tom.enabled = [
      'build',
      'css',
      'default',
      'deploy',
      'html',
      'img',
      'js',
      'server',
      'version',
      'watch'
    ];
  });

  it("Retorno de string", function (done) {
    Tom.do('Sass');
    var expected = _.extend(Tom.Enabled, ['sass']);
    var generated = Tom.Enabled;
    assert.equal(generated, expected);
    done();
  });

  it("Retorno de objeto", function (done) {
    var expected = _.uniq(_.extend(Tom.Enabled, ['sass', 'ts','bower']));
    Tom.do(['Sass', 'Ts']);
    var generated = Tom.Enabled;
    assert.deepEqual(generated, expected);
    done();
  });

});
