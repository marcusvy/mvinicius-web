/**
 * Config.js
 * Global configuration of the-old-man
 * @type {Object}
 */
var path = require('path');

var Config = {
  production: false,
  debug: true,
  bootstrap: [],
  dir: {
    src: '.',
    public: 'app/public',
    assets: 'app/resources',
    build: 'build',
    names: {
      app: 'app',
      compiled: 'compiled',
      css: 'css',
      js: 'js'
    }
  },
  sourcemaps: !this.production,
  autoprefix: {
    enabled: true,
    options: {
      browsers: ['last 2 versions'],
      cascade: false
    }
  }
};

Config.enabled = [
  'build',
  'css',
  'default',
  'deploy',
  'html',
  'img',
  'js',
  'server',
  'version',
  'watch'
];

Config.deploy = {
  "default": {
    "host": "localhost",
    "port": 22,
    "username": "",
    "password": ""
  },
  "sftp": {
    "options": {
      "remotePath": ""
    }
  }
};

Config.size = {
  showFiles: true
};

Config.minify = {
  css: {
    compatibility: 'ie8'
  },
  html: {}
};

Config.plugins = {
  pattern: ['gulp-*', 'del', 'main-bower-files']
};

Config.browserSync = {
  static: true,
  instance: 'The-Old-Man',
  proxy: "http://localhost:80",
  server: {
    baseDir: [
      path.join(Config.dir.src, Config.dir.public),
      path.join(Config.dir.src, Config.dir.assets)
    ],
    directory: false,
    index: "index.html"
  },
  options: {
    port: 8000,
    ui: {
      port: 8001
    },
    browser: ["firefox"],
    open: false,
    online: false,
    injectChanges: true
  }
};

Config.tasks = {
  bower: {
    paths: Config.dir.src,
    checkExistence: true,
    debugging: false,
    includeDev: true
  },
  css: {
    src: '{' + Config.dir.names.app + ',' + Config.dir.names.compiled + '}',
    include: '/**/*.css',
    out: 'css',
    outFile: 'style',
    outFileExt: '.css'
  },
  img: {
    src: Config.dir.names.app,
    out: '',
    include: '/**/*.{gif,png,jpg,jpeg,svg}',
    options: {
      optimizationLevel: 3,
      progressive: false,
      interlaced: false,
      multipass: false
    }
  },
  js: {
    src: '{' + Config.dir.names.app + ',' + Config.dir.names.compiled + '}',
    include: '/**/*.js',
    exclude: '/**/*.{min.js,js.map}',
    out: 'js',
    outFile: 'script',
    outFileExt: '.min.js',
    babel: {
      options: {
        comments: false
      }
    },
    uglify: {
      options: {
        outSourceMap: true,
        mangle: false
      }
    }
  },
  html: {
    src: Config.dir.names.app,
    include: '/**/*.{html,htm}',
    out: '',
    index: 'index'
  },
  sass: {
    src: Config.dir.names.app,
    include: '/**/*.{scss,sass}',
    out: path.join(Config.dir.names.compiled, "sass"),
    options: {
      outputStyle: 'compressed'
    }
  }
};
module.exports = Config;
