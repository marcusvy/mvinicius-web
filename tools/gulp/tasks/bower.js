/**
 * CSS Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var Paths = Tom.Paths;
var taskConfig = Config.tasks.bower;
var source = $.mainBowerFiles(taskConfig);

gulp.task('bower:js', function (cb) {
  gulp.src(source)
    .pipe($.filter('*.js'))
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.init()))
    .pipe($.print())
    .pipe($.concat('vendor.js'))
    //.pipe($.uglify())
    .pipe($.if(Config.sourcemaps, $.sourcemaps.write('.')))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(Paths.js.out))
    .pipe($.size(Tom.onSize('Vendor JS')));
});

gulp.task('bower:css', function (cb) {
  gulp.src(source)
    .pipe($.filter('*.css'))
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.print())
    .pipe($.concat('vendor.css'))
    .pipe($.if(Config.autoprefix.enabled,
      $.autoprefixer(Config.autoprefix.options)))
    .pipe($.minifyCss(Config.minify.css))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(Paths.css.out))
    .pipe($.size(Tom.onSize('Vendor CSS')));
});

gulp.task('bower:font', function (cb) {
  var fontsPath = path.join(Paths.css.out, '../fonts');
  gulp.src(source)
    .pipe($.filter('*.{eot,svg,ttf,woff,woff2,otf}'))
    .pipe($.print())
    .pipe(gulp.dest(fontsPath))
    .pipe($.size(Tom.onSize('Vendor Fonts')));
  console.log(source);
});

gulp.task('bower', ['bower:js', 'bower:css', 'bower:font']);
