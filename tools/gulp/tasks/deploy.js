/**
 * CSS Task
 */
var gulp = require('gulp');
var path = require('path');
var _ = require('underscore');
var Tom = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var taskConfig = Config.deploy;

var sftpConfig = _.extend(taskConfig.sftp.options, taskConfig.default);
sftpConfig.user = taskConfig.default.username;
sftpConfig.pass = taskConfig.default.password;

gulp.task('deploy', function (cb) {
  gulp.src(Config.dir.build + '/**')
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.sftp(sftpConfig))
    .pipe($.plumber.stop())
  cb(null);
});
