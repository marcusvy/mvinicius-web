/**
 * Img Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom  = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var taskPaths = Tom.Paths.img;
var taskConfig = Config.tasks.img;

gulp.task('img', function (cb) {
  gulp.src(taskPaths.src)
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.imagemin(taskConfig.options))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(taskPaths.out))
    .pipe($.size(Tom.onSize('Img: ')));
  cb(null);
});
