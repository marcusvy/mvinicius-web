/**
 * CSS Task
 */
var gulp = require('gulp');
var path = require('path');
var _ = require('underscore');
var Tom = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var srv = {
  server: Config.browserSync.server
};
var prx = {
  proxy: Config.browserSync.proxy
};
var bsOptions = _.extend(
  ((Config.browserSync.static) ? srv : prx),
  Config.browserSync.options
);

/**
 * Task Server
 */
gulp.task('server', function () {
  Tom.BrowserSync.init(bsOptions);
});
