/**
 * CSS Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom = require('./../Tom');


var $ = Tom.$;
var Config = Tom.Config;
var taskPaths = Tom.Paths.css;
var taskConfig = Config.tasks.css;

gulp.task('css', function (cb) {
  gulp.src(taskPaths.src)
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.init()))
    .pipe($.concat(taskConfig.outFile + taskConfig.outFileExt))
    .pipe($.if(Config.autoprefix.enabled,
      $.autoprefixer(Config.autoprefix.options)))
    .pipe($.minifyCss(Config.minify.css))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.write()))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(taskPaths.out))
    .pipe($.if(Tom.BrowserSync.active, Tom.BrowserSync.stream()))
    .pipe($.size(Tom.onSize('CSS')));

  cb(null);
});
