/**
 * HTML Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var taskPaths = Tom.Paths.html;
var taskConfig = Config.tasks.html;

gulp.task('html', function (cb) {
  gulp.src(taskPaths.src)
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.minifyHtml(Config.minify.html))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(taskPaths.out))
    .pipe($.size(Tom.onSize('HTML')))
    .pipe(Tom.BrowserSync.stream());

  cb(null);
});

