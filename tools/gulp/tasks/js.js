/**
 * JS Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var taskPaths = Tom.Paths.js;
var taskConfig = Config.tasks.js;
var babelOptions = taskConfig.babel.options;
var sizeMessage = 'JS: ' + taskConfig.outFile + taskConfig.outFileExt;

gulp.task('js:lint', function (cb) {
  gulp.src(taskPaths.src)
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError());
  cb(null);
});

gulp.task('js', function (cb) {

  gulp.src(taskPaths.src)
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.init()))
    .pipe($.babel(babelOptions))
    .pipe($.concat(taskConfig.outFile + '.js'))
    .pipe($.uglify(taskConfig.uglify.options))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.write('.')))
    .pipe(gulp.dest(taskPaths.out))
    .pipe($.if(Tom.BrowserSync.active, Tom.BrowserSync.stream()))
    .pipe($.size(Tom.onSize('JS')));
  cb(null);
});
