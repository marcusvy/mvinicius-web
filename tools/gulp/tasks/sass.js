/**
 * TS Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom = require('./../Tom');

var $ = Tom.$;
var Config = Tom.Config;
var taskPaths = Tom.Paths.sass;
var taskConfig = Config.tasks.sass;

gulp.task('sass', function (cb) {
  gulp.src(taskPaths.src)
    .pipe($.plumber({
      errorHandler: Tom.onError
    }))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.init()))
    .pipe($.sass(taskConfig.options))
    .pipe($.if(Config.sourcemaps, $.sourcemaps.write()))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(taskPaths.out))
    .pipe($.size(Tom.onSize('SASS compiled')));
  cb(null);
});
