/**
 * Zend Framework Task
 */
var gulp = require('gulp');
var path = require('path');
var Tom = require('./../Tom');


var Config = Tom.Config;
var $ = Tom.$;

gulp.task('zf2', function (cb) {
  gulp.src(path.resolve('public/index.php'))
    .pipe($.print())
    .pipe($.if(Tom.BrowserSync.active, Tom.BrowserSync.stream()));
  cb();
});
