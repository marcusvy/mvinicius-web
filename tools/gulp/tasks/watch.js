/**
 * Register watch task
 * @return {Object}
 */
var gulp = require('gulp');
var Tom = require('./../Tom');

gulp.task('watch', function () {
  var Paths = Tom.Paths;

  gulp.watch('bower.json', ['bower']);
  gulp.watch('package.json', ['version']);
  gulp.watch('module/**/*.{php,phtml}', ['zf2']);

  gulp.watch(Paths.js.src, ['js']);
  gulp.watch(Paths.css.src, ['css']);
  gulp.watch(Paths.html.src, ['html']);
  gulp.watch(Paths.sass.src, ['sass', 'css']);
  gulp.watch(Paths.img.src, ['img', Tom.BrowserSync.reload]);

});
