/**
 * CSS Task
 */
var gulp = require('gulp');
var path = require('path');
var _ = require('underscore');
var Tom = require('./../Tom');
var Notification = require('./../lib/Notification');

var Config = Tom.Config;
var version = Config.global.node.version;
var message = 'Version: ' + version

gulp.task('version', function () {
  gulp.src('gulpfile.js')
    .pipe(Notification.message(message));
});
