/**
 * Make build task from parameters
 * @description Use exports.build options
 * - boot
 * - before
 * - default
 * - after
 * - final
 * @return {Object}
 */
var gulp = require('gulp');
var _ = require('underscore');
var Tom = require('./../Tom');

gulp.task('build', Tom.Build , function (cb) {
  cb(null);
});
