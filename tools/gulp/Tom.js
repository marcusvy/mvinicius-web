'use strict';

/**
 * Gulp
 * @author Marcus Vinícius da R G Cardoso <marcusvy@gmail.com>
 */
var gulp = require('gulp');
var path = require('path');
var _ = require('underscore');
var ConfigProvider = require('./lib/Provider/Config');
var Notification = require('./lib/Notification');

var Tom = function () {
  var bsInstance;
  var bowerConfigFile = path.resolve('bower.json');
  var nodeConfigFile = path.resolve('package.json');

  this.Config = ConfigProvider.load(require('./Config'));
  this.Config.global = {};
  this.Config.global.bower = require(bowerConfigFile);
  this.Config.global.node = require(nodeConfigFile);
  this.Paths = require('./lib/Paths')(this.Config);
  this.$ = require('gulp-load-plugins')(this.Config.plugins);
  bsInstance = this.Config.browserSync.instance;
  this.BrowserSync = require('browser-sync').create(bsInstance);
  this.Build = ['bower','sass','img','js','css'];
};

Tom.prototype = {
  /**
   * Error handler for plumber
   * @param  {object}  e    Error object
   * @param  {boolean} end  If true emit the end command
   * @return {null}         Return nothing
   */
  onError: function (e) {
    Notification.error(e, 'Ops!');
    this.emit('end');
  },

  /**
   * Size reporter
   * @param  {string} title  Title reporter
   * @param  {Object} Config Configuration file
   * @return {Object}        Gulp-size configuration object
   */
  onSize: function (title) {
    return _.extend({}, this.Config.size, {
      title: title
    });
  }
};

var inst = new Tom();
module.exports = inst;
