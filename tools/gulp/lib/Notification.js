/**
 * Notification.js
 * Gera notificações
 * @type {Object}
 */
var notify = require('gulp-notify');

var Notification = function() {
  this.title = 'TOM: The Old Man';
};

Notification.prototype = {

  log: notify.withReporter(function(opt, cb) {
    console.log("Debug: ", opt.title);
    console.log(opt.message);
    cb();
  }),

  error: function(e, message) {
    notify.onError({
      title: this.title,
      message: message + ': \n<%= error.message %>',
      icon: __dirname + '/../icon/error.png'
    });
    console.log(e.message);
  },

  fileError: function(e, title) {
    notify.onError({
      title: title,
      subtitle: "subs",
      message: '<%= error.diagnostic.messageText %>',
      icon: __dirname + '/../icon/error.png'
    })(e);
    //console.log(e.message);
  },

  forPassedTests: function(framework) {
    return notify({
      title: 'Verde!',
      message: 'Seu ' + framework + ' passou nos testes!',
      icon: __dirname + '/../icon/ok.png',
      onLast: true
    });
  },

  forFailedTests: function(e, framework) {
    return notify.onError({
      title: 'Vermelho!',
      message: 'Seu ' + framework + ' teste falhou!',
      icon: __dirname + '/../icon/error.png'
    })(e);
  },

  message: function(message) {
    return notify({
      title: this.title,
      message: message,
      icon: __dirname + '/../icon/icon.png',
      onLast: true
    });
  }

};

module.exports = new Notification();
