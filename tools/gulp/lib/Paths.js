/**
 * Path
 * @author Marcus Vinícius da R G Cardoso <marcusvy@gmail.com>
 * @return {Object}
 */

var path = require('path');


/**
 * Cria um caminho
 * @param  {Object} Config       Config Objeto de Configurações
 * @param  {string} referenceDir Diretório de referência (Ex.: public, assets)
 * @param  {string} dir          Dirtetório final
 * @return {string}
 */
function make(Config, referenceDir, dir) {
  return path.join(Config.dir.src, referenceDir, dir);
}

/**
 * Cria o caminho especificado
 * @param  {Object} Config Objeto de Configurações
 * @param  {string} referenceDir Diretório de referência (Ex.: public, assets)
 * @param  {strign} task   Tarefa a ser pesquisada em Config.js
 * @return {Array}
 */
function makeSrc(Config, referenceDir, task) {
  var relPath = [];
  var configTask = Config.tasks.hasOwnProperty(task) ? Config.tasks[task + ''] : false;

  if (configTask.hasOwnProperty('src') && configTask.hasOwnProperty('include')) {
    relPath.push(path.join(
      Config.dir.src, referenceDir, configTask.src, configTask.include
    ));
  }
  if (configTask.hasOwnProperty('src') && configTask.hasOwnProperty('exclude') && configTask.exclude != "") {
    relPath.push(path.join(
      Config.dir.src, referenceDir, configTask.src, configTask.exclude
    ));
  }
  return relPath;
};

/**
 * Cria caminho de saída
 * @param  {Object} Config Objeto de Configurações
 * @param  {string} referenceDir Diretório de referência (Ex.: public, assets)
 * @param  {strign} task   Tarefa a ser pesquisada em Config.js
 * @return {string}
 */
function makeOut(Config, referenceDir, task) {
  var relPath = '';
  var configTask = Config.tasks.hasOwnProperty(task) ? Config.tasks[task + ''] : false;

  if (configTask.hasOwnProperty('out')) {
    relPath = path.join(Config.dir.src, referenceDir, configTask.out);
  }
  return relPath;
}



module.exports = function(Config) {
  var paths = {
    css: {
      src: makeSrc(Config, Config.dir.assets, 'css'),
      out: makeOut(Config, Config.dir.public, 'css')
    },
    html: {
      src: makeSrc(Config, Config.dir.assets, 'html'),
      out: makeOut(Config, Config.dir.public, 'html')
    },
    img: {
      src: makeSrc(Config, Config.dir.assets, 'img'),
      out: makeOut(Config, Config.dir.public, 'img')
    },
    favicon: {
      src: makeSrc(Config, Config.dir.assets, 'favicon'),
      out: makeOut(Config, Config.dir.public, 'favicon')
    },
    js: {
      src: makeSrc(Config, Config.dir.assets, 'js'),
      out: makeOut(Config, Config.dir.public, 'js')
    },
    sass: {
      src: makeSrc(Config, Config.dir.assets, 'sass'),
      out: makeOut(Config, Config.dir.assets, 'sass')
    }
  };
  return paths;
};
