/**
 * ConfigProvider.js
 * Provedor de configurações
 * @type {Object}
 */

var _ = require('underscore');
var fs = require("fs");
var path = require('path');
var deepExtend = require('extend');

/**
 * Provedor de configurações
 * @param  {Object} Config Default config file
 * @return {Object}
 */
var ConfigProvider = function() {
  this.file = 'the-old-man.json';
  this.config = {};
};

ConfigProvider.prototype = {
  /**
   * Extende a configuração caso exista o arquivo 'the-old-man.json'
   * @returns {boolean}
   */
  extendFromJsonFile: function() {
    var configFile = path.join(process.cwd(), this.file);
    if (fs.existsSync(configFile)) {
      var jsonOpt = {
        encoding: 'utf-8'
      };
      var configFromJson = fs.readFileSync(configFile, jsonOpt);
      this.config = deepExtend(true, this.config, JSON.parse(configFromJson));
      return true;
    }
    return false;
  },

  /**
   * Carrega a configuração adequada
   * @returns {}
   */
  load: function(Config) {
    this.config = Config;
    this.extendFromJsonFile();
    return this.config;
  }
};

module.exports = new ConfigProvider();
