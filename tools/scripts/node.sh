#!/usr/bin/env bash
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs
sudo chown -R vagrant /usr/lib/node_modules/
mkdir /home/vagrant/npm-global
chown -R vagrant /home/vagrant/npm-global
npm config set prefix '/home/vagrant/npm-global'
export PATH=/home/vagrant/npm-global/bin:$PATH
source /home/vagrant/.profile
npm cache clean
npm install -g gulp
npm install -g bower
npm install -g socket.io
npm install -g browser-sync
