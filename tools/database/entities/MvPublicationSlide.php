<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvPublicationSlide
 *
 * @ORM\Table(name="mv_publication_slide")
 * @ORM\Entity
 */
class MvPublicationSlide
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="href", type="string", length=255, nullable=true)
     */
    private $href;

    /**
     * @var integer
     *
     * @ORM\Column(name="midia", type="integer", nullable=false)
     */
    private $midia;

    /**
     * @var boolean
     *
     * @ORM\Column(name="midia_type", type="boolean", nullable=false)
     */
    private $midiaType;


}

