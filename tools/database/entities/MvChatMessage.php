<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvChatMessage
 *
 * @ORM\Table(name="mv_chat_message", indexes={@ORM\Index(name="fk_chatmessages_sender", columns={"sender"}), @ORM\Index(name="fk_chatmessages_receiver", columns={"receiver"})})
 * @ORM\Entity
 */
class MvChatMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \MvUser
     *
     * @ORM\ManyToOne(targetEntity="MvUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="receiver", referencedColumnName="id")
     * })
     */
    private $receiver;

    /**
     * @var \MvUser
     *
     * @ORM\ManyToOne(targetEntity="MvUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sender", referencedColumnName="id")
     * })
     */
    private $sender;


}

