<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvHelpdeskProblem
 *
 * @ORM\Table(name="mv_helpdesk_problem", indexes={@ORM\Index(name="fk_helpdeskproblems_category", columns={"category"}), @ORM\Index(name="fk_helpdeskproblems_solution", columns={"solution"})})
 * @ORM\Entity
 */
class MvHelpdeskProblem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var \MvHelpdeskProblemCategory
     *
     * @ORM\ManyToOne(targetEntity="MvHelpdeskProblemCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \MvHelpdeskSolution
     *
     * @ORM\ManyToOne(targetEntity="MvHelpdeskSolution")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="solution", referencedColumnName="id")
     * })
     */
    private $solution;


}

