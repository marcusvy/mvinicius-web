<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvPublicationsPages
 *
 * @ORM\Table(name="mv_publications_pages")
 * @ORM\Entity
 */
class MvPublicationsPages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;


}

