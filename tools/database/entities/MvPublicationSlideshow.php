<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvPublicationSlideshow
 *
 * @ORM\Table(name="mv_publication_slideshow", indexes={@ORM\Index(name="fk_publicationsslideshows_local", columns={"local"})})
 * @ORM\Entity
 */
class MvPublicationSlideshow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slides", type="string", length=255, nullable=false)
     */
    private $slides;

    /**
     * @var \MvPublicationLocal
     *
     * @ORM\ManyToOne(targetEntity="MvPublicationLocal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="local", referencedColumnName="id")
     * })
     */
    private $local;


}

