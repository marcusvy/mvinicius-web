<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvHelpdeskChat
 *
 * @ORM\Table(name="mv_helpdesk_chat", indexes={@ORM\Index(name="fk_helpdeskchat_client", columns={"client"}), @ORM\Index(name="fk_helpdeskchat_consultant", columns={"consultant"})})
 * @ORM\Entity
 */
class MvHelpdeskChat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \MvClient
     *
     * @ORM\ManyToOne(targetEntity="MvClient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \MvUser
     *
     * @ORM\ManyToOne(targetEntity="MvUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="consultant", referencedColumnName="id")
     * })
     */
    private $consultant;


}

