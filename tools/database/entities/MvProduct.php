<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvProduct
 *
 * @ORM\Table(name="mv_product", indexes={@ORM\Index(name="fk_products_img", columns={"image"}), @ORM\Index(name="fk_products_slideshow", columns={"slideshow"}), @ORM\Index(name="fk_products_category", columns={"category"})})
 * @ORM\Entity
 */
class MvProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=45, nullable=false)
     */
    private $amount;

    /**
     * @var \MvProductCategory
     *
     * @ORM\ManyToOne(targetEntity="MvProductCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \MvMidiaImage
     *
     * @ORM\ManyToOne(targetEntity="MvMidiaImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image", referencedColumnName="id")
     * })
     */
    private $image;

    /**
     * @var \MvPublicationSlideshow
     *
     * @ORM\ManyToOne(targetEntity="MvPublicationSlideshow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slideshow", referencedColumnName="id")
     * })
     */
    private $slideshow;


}

