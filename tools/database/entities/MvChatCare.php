<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvChatCare
 *
 * @ORM\Table(name="mv_chat_care", indexes={@ORM\Index(name="fk_chatcare_sender", columns={"sender"})})
 * @ORM\Entity
 */
class MvChatCare
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_email", type="string", length=255, nullable=false)
     */
    private $receiverEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_name", type="string", length=255, nullable=true)
     */
    private $receiverName;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \MvUser
     *
     * @ORM\ManyToOne(targetEntity="MvUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sender", referencedColumnName="id")
     * })
     */
    private $sender;


}

