<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvHelpdeskProblemCategory
 *
 * @ORM\Table(name="mv_helpdesk_problem_category")
 * @ORM\Entity
 */
class MvHelpdeskProblemCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority;


}

