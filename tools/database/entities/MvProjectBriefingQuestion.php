<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvProjectBriefingQuestion
 *
 * @ORM\Table(name="mv_project_briefing_question")
 * @ORM\Entity
 */
class MvProjectBriefingQuestion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255, nullable=false)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="possible_asswers", type="string", length=255, nullable=true)
     */
    private $possibleAsswers;


}

