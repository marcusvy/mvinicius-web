<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvClient
 *
 * @ORM\Table(name="mv_client", uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})}, indexes={@ORM\Index(name="fk_client_avatar", columns={"avatar"})})
 * @ORM\Entity
 */
class MvClient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="birth_place", type="string", length=50, nullable=true)
     */
    private $birthPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=50, nullable=true)
     */
    private $nationality;

    /**
     * @var string
     *
     * @ORM\Column(name="sociallinks", type="text", length=65535, nullable=true)
     */
    private $sociallinks;

    /**
     * @var string
     *
     * @ORM\Column(name="address_number", type="string", length=20, nullable=true)
     */
    private $addressNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="address_street", type="string", length=100, nullable=true)
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="address_district", type="string", length=100, nullable=true)
     */
    private $addressDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="adress_city", type="string", length=50, nullable=true)
     */
    private $adressCity;

    /**
     * @var string
     *
     * @ORM\Column(name="adress_estate", type="string", length=50, nullable=true)
     */
    private $adressEstate;

    /**
     * @var string
     *
     * @ORM\Column(name="adress_country", type="string", length=50, nullable=true)
     */
    private $adressCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_personal", type="string", length=12, nullable=true)
     */
    private $phonePersonal;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_home", type="string", length=12, nullable=true)
     */
    private $phoneHome;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_work", type="string", length=12, nullable=true)
     */
    private $phoneWork;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=12, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_rg", type="string", length=45, nullable=true)
     */
    private $docRg;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_cpf", type="string", length=15, nullable=true)
     */
    private $docCpf;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_cnh", type="string", length=15, nullable=true)
     */
    private $docCnh;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_votereg", type="string", length=45, nullable=true)
     */
    private $docVotereg;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=45, nullable=true)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="company_doc_ie", type="string", length=45, nullable=true)
     */
    private $companyDocIe;

    /**
     * @var string
     *
     * @ORM\Column(name="company_cnpj", type="string", length=14, nullable=true)
     */
    private $companyCnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="company_departament", type="string", length=255, nullable=true)
     */
    private $companyDepartament;

    /**
     * @var string
     *
     * @ORM\Column(name="company_adress_street", type="string", length=100, nullable=true)
     */
    private $companyAdressStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="company_adress_number", type="string", length=20, nullable=true)
     */
    private $companyAdressNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="company_adress_district", type="string", length=100, nullable=true)
     */
    private $companyAdressDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="company_adress_city", type="string", length=50, nullable=true)
     */
    private $companyAdressCity;

    /**
     * @var string
     *
     * @ORM\Column(name="company_adress_estate", type="string", length=50, nullable=true)
     */
    private $companyAdressEstate;

    /**
     * @var string
     *
     * @ORM\Column(name="company_adress_country", type="string", length=50, nullable=true)
     */
    private $companyAdressCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="company_postalcode", type="string", length=12, nullable=true)
     */
    private $companyPostalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="company_phone_cel", type="string", length=45, nullable=true)
     */
    private $companyPhoneCel;

    /**
     * @var string
     *
     * @ORM\Column(name="company_phone_comercial", type="string", length=255, nullable=true)
     */
    private $companyPhoneComercial;

    /**
     * @var string
     *
     * @ORM\Column(name="email_primary", type="string", length=255, nullable=true)
     */
    private $emailPrimary;

    /**
     * @var string
     *
     * @ORM\Column(name="email_secondary", type="string", length=45, nullable=true)
     */
    private $emailSecondary;

    /**
     * @var \MvMidiaImage
     *
     * @ORM\ManyToOne(targetEntity="MvMidiaImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="avatar", referencedColumnName="id")
     * })
     */
    private $avatar;


}

