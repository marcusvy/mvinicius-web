<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvProject
 *
 * @ORM\Table(name="mv_project", indexes={@ORM\Index(name="fk_projects_client", columns={"client"}), @ORM\Index(name="fk_projects_request", columns={"sell"}), @ORM\Index(name="fk_projects_briefing", columns={"briefing"}), @ORM\Index(name="fk_projects_consultant", columns={"consultant"}), @ORM\Index(name="fk_projects_contract", columns={"contract"})})
 * @ORM\Entity
 */
class MvProject
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=65535, nullable=true)
     */
    private $observation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="deal_payment", type="text", length=65535, nullable=true)
     */
    private $dealPayment;

    /**
     * @var \MvProjectBriefing
     *
     * @ORM\ManyToOne(targetEntity="MvProjectBriefing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="briefing", referencedColumnName="id")
     * })
     */
    private $briefing;

    /**
     * @var \MvClient
     *
     * @ORM\ManyToOne(targetEntity="MvClient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \MvUser
     *
     * @ORM\ManyToOne(targetEntity="MvUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="consultant", referencedColumnName="id")
     * })
     */
    private $consultant;

    /**
     * @var \MvContract
     *
     * @ORM\ManyToOne(targetEntity="MvContract")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contract", referencedColumnName="id")
     * })
     */
    private $contract;

    /**
     * @var \MvProjectSell
     *
     * @ORM\ManyToOne(targetEntity="MvProjectSell")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sell", referencedColumnName="id")
     * })
     */
    private $sell;


}

