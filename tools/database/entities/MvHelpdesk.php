<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvHelpdesk
 *
 * @ORM\Table(name="mv_helpdesk", indexes={@ORM\Index(name="fk_helpdesk_client", columns={"client"}), @ORM\Index(name="fk_helpdesk_problem", columns={"problem"})})
 * @ORM\Entity
 */
class MvHelpdesk
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", length=65535, nullable=false)
     */
    private $observation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var \MvClient
     *
     * @ORM\ManyToOne(targetEntity="MvClient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \MvHelpdeskProblem
     *
     * @ORM\ManyToOne(targetEntity="MvHelpdeskProblem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="problem", referencedColumnName="id")
     * })
     */
    private $problem;


}

