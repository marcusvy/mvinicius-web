<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvProjectSell
 *
 * @ORM\Table(name="mv_project_sell")
 * @ORM\Entity
 */
class MvProjectSell
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=15, nullable=false)
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", precision=10, scale=0, nullable=false)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="products", type="text", length=65535, nullable=false)
     */
    private $products;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_required", type="datetime", nullable=false)
     */
    private $timeRequired;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_delivery", type="datetime", nullable=false)
     */
    private $timeDelivery;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_max", type="datetime", nullable=true)
     */
    private $timeMax;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_fixed", type="datetime", nullable=true)
     */
    private $timeFixed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=255, nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="deal_payment", type="text", length=65535, nullable=true)
     */
    private $dealPayment;


}

