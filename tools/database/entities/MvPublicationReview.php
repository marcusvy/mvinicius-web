<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvPublicationReview
 *
 * @ORM\Table(name="mv_publication_review", indexes={@ORM\Index(name="fk_publicationsreviews_publication", columns={"publication"}), @ORM\Index(name="fk_publicationsreviews_img", columns={"featured_image"})})
 * @ORM\Entity
 */
class MvPublicationReview
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="lead", type="text", length=65535, nullable=true)
     */
    private $lead;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;

    /**
     * @var \MvMidiaImage
     *
     * @ORM\ManyToOne(targetEntity="MvMidiaImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="featured_image", referencedColumnName="id")
     * })
     */
    private $featuredImage;

    /**
     * @var \MvPublicationNews
     *
     * @ORM\ManyToOne(targetEntity="MvPublicationNews")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="publication", referencedColumnName="id")
     * })
     */
    private $publication;


}

