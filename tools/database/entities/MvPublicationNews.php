<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvPublicationNews
 *
 * @ORM\Table(name="mv_publication_news", indexes={@ORM\Index(name="fk_publicationsnews_user", columns={"author"}), @ORM\Index(name="fk_publicationsnews_revision", columns={"revision"})})
 * @ORM\Entity
 */
class MvPublicationNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="source_name", type="string", length=255, nullable=true)
     */
    private $sourceName;

    /**
     * @var string
     *
     * @ORM\Column(name="source_url", type="string", length=255, nullable=true)
     */
    private $sourceUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedat", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime", nullable=false)
     */
    private $publishedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiredAt", type="datetime", nullable=true)
     */
    private $expiredat;

    /**
     * @var \MvPublicationReview
     *
     * @ORM\ManyToOne(targetEntity="MvPublicationReview")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="revision", referencedColumnName="id")
     * })
     */
    private $revision;

    /**
     * @var \MvUser
     *
     * @ORM\ManyToOne(targetEntity="MvUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="author", referencedColumnName="id")
     * })
     */
    private $author;


}

