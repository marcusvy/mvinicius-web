<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvProductCategory
 *
 * @ORM\Table(name="mv_product_category")
 * @ORM\Entity
 */
class MvProductCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;


}

