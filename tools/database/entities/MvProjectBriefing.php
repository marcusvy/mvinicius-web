<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvProjectBriefing
 *
 * @ORM\Table(name="mv_project_briefing", indexes={@ORM\Index(name="fk_projectsbriefing_question", columns={"question"})})
 * @ORM\Entity
 */
class MvProjectBriefing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=45, nullable=false)
     */
    private $answer;

    /**
     * @var \MvProjectBriefingQuestion
     *
     * @ORM\ManyToOne(targetEntity="MvProjectBriefingQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question", referencedColumnName="id")
     * })
     */
    private $question;


}

