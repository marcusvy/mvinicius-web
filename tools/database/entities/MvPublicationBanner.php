<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * MvPublicationBanner
 *
 * @ORM\Table(name="mv_publication_banner", indexes={@ORM\Index(name="fk_publicationsbanner_local", columns={"local"})})
 * @ORM\Entity
 */
class MvPublicationBanner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="href", type="string", length=255, nullable=true)
     */
    private $href;

    /**
     * @var string
     *
     * @ORM\Column(name="resource", type="string", length=255, nullable=true)
     */
    private $resource;

    /**
     * @var integer
     *
     * @ORM\Column(name="click", type="integer", nullable=true)
     */
    private $click;

    /**
     * @var \MvPublicationLocal
     *
     * @ORM\ManyToOne(targetEntity="MvPublicationLocal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="local", referencedColumnName="id")
     * })
     */
    private $local;


}

