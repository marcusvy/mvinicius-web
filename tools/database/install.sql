-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mvcosmos
-- -----------------------------------------------------
-- ERP MVCosmos

-- -----------------------------------------------------
-- Schema mvcosmos
--
-- ERP MVCosmos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mvcosmos` DEFAULT CHARACTER SET utf8 ;
USE `mvcosmos` ;

-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_users_acl`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_users_acl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL COMMENT 'Nome para o nível de acesso',
  `role` VARCHAR(45) NOT NULL COMMENT 'Regra a ser adotada',
  `resource` VARCHAR(45) NOT NULL COMMENT 'Recurso a ser protegido',
  `active` TINYINT(1) NULL COMMENT 'Ativação da regra',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_midias_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_midias_image` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL COMMENT 'Título da imagem',
  `description` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Descrição',
  `uri` TEXT NOT NULL COMMENT 'Local da imagem',
  `width` INT(5) NOT NULL COMMENT 'Metadata',
  `height` INT(5) NOT NULL COMMENT 'Metadata',
  `format` VARCHAR(5) NULL COMMENT 'Extensão utilizada. Ex.: jpg;png',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_users_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_users_perfil` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL COMMENT 'Nome completo',
  `birthday` DATETIME NULL,
  `birth_place` VARCHAR(50) NULL,
  `nationality` VARCHAR(50) NULL,
  `address_number` VARCHAR(20) NULL,
  `address_street` VARCHAR(100) NULL,
  `address_district` VARCHAR(100) NULL COMMENT 'Endereço',
  `phone_personal` CHAR(12) NULL COMMENT 'Array serializado de telefones',
  `phone_home` CHAR(12) NULL COMMENT '559900009999',
  `phone_work` CHAR(12) NULL,
  `postal_code` CHAR(12) NULL COMMENT 'CEP',
  `city` VARCHAR(50) NULL COMMENT 'Naturalidade',
  `state` VARCHAR(50) NULL COMMENT 'Nacionalidade',
  `country` VARCHAR(50) NULL,
  `sociallinks` TEXT NULL COMMENT 'Links serializados para o facebook, google plus, linkedin,',
  `avatar` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_usersperfil_avatar` (`avatar` ASC),
  CONSTRAINT `fk_usersperfil_avatar`
    FOREIGN KEY (`avatar`)
    REFERENCES `mvcosmos`.`mv_midias_image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_users_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_users_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificação única',
  `credential` VARCHAR(80) NOT NULL COMMENT 'Nome do login',
  `password` VARCHAR(80) NOT NULL COMMENT 'Seha de acesso',
  `salt` VARCHAR(80) NOT NULL,
  `active` TINYINT(1) NULL COMMENT 'Status de ativação',
  `createdat` DATETIME NULL,
  `updatedat` DATETIME NULL,
  `fk_acl` INT(11) NULL,
  `fk_perfil` INT(11) NULL,
  `status` VARCHAR(45) NULL COMMENT 'Utilização de espaço para lixeira',
  PRIMARY KEY (`id`),
  INDEX `fk_usersuser_acl` (`fk_acl` ASC),
  INDEX `fk_usersuser_perfil` (`fk_perfil` ASC),
  CONSTRAINT `fk_usersuser_acl`
    FOREIGN KEY (`fk_acl`)
    REFERENCES `mvcosmos`.`mv_users_acl` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_usersuser_perfil`
    FOREIGN KEY (`fk_perfil`)
    REFERENCES `mvcosmos`.`mv_users_perfil` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_midias_video`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_midias_video` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NULL,
  `description` VARCHAR(255) NULL,
  `type` VARCHAR(50) NOT NULL COMMENT 'local de armazenamento para geração do reprodutor de vídeo. Ex.: youtube; vimeo',
  `duration` TIME NULL,
  `width` INT(5) NOT NULL COMMENT 'Largura do video',
  `height` INT(5) NOT NULL COMMENT 'Altura do vídeo',
  `uri` VARCHAR(255) NULL COMMENT 'Local completo de localização',
  `url_code` VARCHAR(255) NULL COMMENT 'Código de registro para o vídeo',
  `format` VARCHAR(5) NULL COMMENT 'Extensão utilizada. Ex.: avi;',
  `size` INT(4) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_midias_audio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_midias_audio` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `type` VARCHAR(50) NOT NULL COMMENT 'Local de armazenamento para geração do reprodutor de vídeo. Ex.: youtube; vimeo',
  `duration` TIME NULL COMMENT 'Largura do áudio',
  `uri` VARCHAR(255) NULL COMMENT 'Local completo de localização',
  `url_code` VARCHAR(255) NULL COMMENT 'Código de registro para utilização externa',
  `format` VARCHAR(5) NULL COMMENT 'Extensão utilizada. Ex.: ogg; mp3',
  `size` INT(5) NULL COMMENT 'Altura do vídeo',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `directive` VARCHAR(50) NULL,
  `description` VARCHAR(255) NULL,
  `value` VARCHAR(255) NULL,
  `active` TINYINT(1) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_reviews` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `publication` INT(11) NOT NULL COMMENT 'Id da publicação referente',
  `featured_image` INT(11) NULL DEFAULT 0,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `lead` TEXT NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `status` VARCHAR(50) NULL COMMENT 'Estato, aprovado, recusado',
  PRIMARY KEY (`id`),
  INDEX `fk_publicationsreviews_publication` (`publication` ASC),
  INDEX `fk_publicationsreviews_img` (`featured_image` ASC),
  CONSTRAINT `fk_publicationsreviews_publication`
    FOREIGN KEY (`publication`)
    REFERENCES `mvcosmos`.`mv_publications_news` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_publicationsreviews_img`
    FOREIGN KEY (`featured_image`)
    REFERENCES `mvcosmos`.`mv_midias_image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_news`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_news` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `author` INT(11) NOT NULL COMMENT 'Identificação e autor da publicação.',
  `revision` INT(11) NULL DEFAULT 0 COMMENT 'Atualizado para a última revisão',
  `source_name` VARCHAR(255) NULL COMMENT 'Fonte da notícia',
  `source_url` VARCHAR(255) NULL COMMENT 'Url para link da notícia',
  `updatedat` DATETIME NULL,
  `createdat` DATETIME NULL,
  `publishedAt` DATETIME NOT NULL,
  `expiredAt` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_publicationsnews_user` (`author` ASC),
  INDEX `fk_publicationsnews_revision` (`revision` ASC),
  CONSTRAINT `fk_publicationsnews_user`
    FOREIGN KEY (`author`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_publicationsnews_revision`
    FOREIGN KEY (`revision`)
    REFERENCES `mvcosmos`.`mv_publications_reviews` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_locals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_locals` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL COMMENT 'Descrição ou título',
  `width` INT(5) NOT NULL,
  `height` INT(5) NOT NULL,
  `price` FLOAT(11) NOT NULL COMMENT 'Preço do local',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_banner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_banner` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `local` INT(11) NOT NULL COMMENT 'Espaço para publicação',
  `href` VARCHAR(255) NULL COMMENT 'Link para execução',
  `resource` VARCHAR(255) NULL COMMENT 'Url do swf ou img',
  `click` INT(11) NULL COMMENT 'contagem de clicks',
  PRIMARY KEY (`id`),
  INDEX `fk_publicationsbanner_local` (`local` ASC),
  CONSTRAINT `fk_publicationsbanner_local`
    FOREIGN KEY (`local`)
    REFERENCES `mvcosmos`.`mv_publications_locals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_slides`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_slides` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL,
  `href` VARCHAR(255) NULL COMMENT 'Link para clique',
  `midia` INT(11) NOT NULL COMMENT 'Midia utilizada',
  `midia_type` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0: imagem; 1:video; 2: áudio',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_slideshows`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_slideshows` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `local` INT(11) NOT NULL,
  `slides` VARCHAR(255) NOT NULL COMMENT 'Array serializado de slides contendo os id\'s dos slides',
  PRIMARY KEY (`id`),
  INDEX `fk_publicationsslideshows_local` (`local` ASC),
  CONSTRAINT `fk_publicationsslideshows_local`
    FOREIGN KEY (`local`)
    REFERENCES `mvcosmos`.`mv_publications_locals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_publications_pages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_publications_pages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL COMMENT 'Título da página',
  `content` TEXT NOT NULL COMMENT 'conteúdo a ser apresentado na página',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_client` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL COMMENT 'Nome completo',
  `avatar` INT(11) NULL DEFAULT 0,
  `birthday` DATETIME NULL,
  `birth_place` VARCHAR(50) NULL,
  `nationality` VARCHAR(50) NULL,
  `sociallinks` TEXT NULL COMMENT 'Links serializados para o facebook, google plus, linkedin,',
  `address_number` VARCHAR(20) NULL,
  `address_street` VARCHAR(100) NULL,
  `address_district` VARCHAR(100) NULL COMMENT 'Endereço',
  `adress_city` VARCHAR(50) NULL COMMENT 'Naturalidade',
  `adress_estate` VARCHAR(50) NULL COMMENT 'Nacionalidade',
  `adress_country` VARCHAR(50) NULL,
  `phone_personal` CHAR(12) NULL COMMENT 'Array serializado de telefones',
  `phone_home` CHAR(12) NULL COMMENT '559900009999',
  `phone_work` CHAR(12) NULL,
  `postal_code` CHAR(12) NULL COMMENT 'CEP',
  `doc_rg` VARCHAR(45) NULL COMMENT 'Registro Geral',
  `doc_cpf` VARCHAR(15) NULL COMMENT 'Cadastro de pessoa física',
  `doc_cnh` VARCHAR(15) NULL COMMENT 'Carteira nacional de habilitação',
  `doc_votereg` VARCHAR(45) NULL COMMENT 'Título de eleitor',
  `company_name` VARCHAR(45) NULL,
  `company_doc_ie` VARCHAR(45) NULL COMMENT 'Inscrição estadual',
  `company_cnpj` CHAR(14) NULL COMMENT 'Cadastro de pessoa jurídica',
  `company_departament` VARCHAR(255) NULL,
  `company_adress_street` VARCHAR(100) NULL,
  `company_adress_number` VARCHAR(20) NULL,
  `company_adress_district` VARCHAR(100) NULL,
  `company_adress_city` VARCHAR(50) NULL,
  `company_adress_estate` VARCHAR(50) NULL,
  `company_adress_country` VARCHAR(50) NULL,
  `company_postalcode` CHAR(12) NULL,
  `company_phone_cel` VARCHAR(45) NULL,
  `company_phone_comercial` VARCHAR(255) NULL,
  `email_primary` VARCHAR(255) NULL,
  `email_secondary` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_client_avatar` (`avatar` ASC),
  CONSTRAINT `fk_client_avatar`
    FOREIGN KEY (`avatar`)
    REFERENCES `mvcosmos`.`mv_midias_image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_projects_sells`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_projects_sells` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(15) NOT NULL COMMENT 'closed; opened',
  `total` FLOAT(5) NOT NULL COMMENT 'Total da venda (pedido)',
  `products` TEXT NOT NULL COMMENT 'Array serializado de produtos do tipo: (id:quantidade)',
  `time_required` DATETIME NOT NULL COMMENT 'Prazo requirido',
  `time_delivery` DATETIME NOT NULL COMMENT 'Prazo de entrega',
  `time_max` DATETIME NULL COMMENT 'Prazo máximo',
  `time_fixed` DATETIME NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `observation` VARCHAR(255) NULL,
  `deal_payment` TEXT NULL COMMENT 'Acordo de pagamento',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_projects_briefing_questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_projects_briefing_questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(255) NOT NULL,
  `possible_asswers` VARCHAR(255) NULL COMMENT 'Array serializado de respostas',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_projects_briefing`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_projects_briefing` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question` INT(11) NOT NULL,
  `answer` VARCHAR(45) NOT NULL COMMENT 'Resposta',
  PRIMARY KEY (`id`),
  INDEX `fk_projectsbriefing_question` (`question` ASC),
  CONSTRAINT `fk_projectsbriefing_question`
    FOREIGN KEY (`question`)
    REFERENCES `mvcosmos`.`mv_projects_briefing_questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_contracts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_contracts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` TEXT NOT NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_projects`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_projects` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NULL,
  `consultant` INT(11) NULL,
  `client` INT(11) NOT NULL,
  `sell` INT(11) NOT NULL,
  `briefing` INT(11) NULL,
  `contract` INT(11) NULL,
  `observation` TEXT NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `status` VARCHAR(45) NULL COMMENT 'O estado: open; close; wait; frozen',
  `deal_payment` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_projects_client` (`client` ASC),
  INDEX `fk_projects_request` (`sell` ASC),
  INDEX `fk_projects_briefing` (`briefing` ASC),
  INDEX `fk_projects_consultant` (`consultant` ASC),
  INDEX `fk_projects_contract` (`contract` ASC),
  CONSTRAINT `fk_projects_client`
    FOREIGN KEY (`client`)
    REFERENCES `mvcosmos`.`mv_client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_projects_request`
    FOREIGN KEY (`sell`)
    REFERENCES `mvcosmos`.`mv_projects_sells` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_projects_briefing`
    FOREIGN KEY (`briefing`)
    REFERENCES `mvcosmos`.`mv_projects_briefing` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_projects_consultant`
    FOREIGN KEY (`consultant`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_projects_contract`
    FOREIGN KEY (`contract`)
    REFERENCES `mvcosmos`.`mv_contracts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_products_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_products_categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_products` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL COMMENT 'Nome',
  `category` INT(11) NOT NULL COMMENT 'Categoria',
  `price` FLOAT(5) NOT NULL COMMENT 'Preço',
  `amount` VARCHAR(45) NOT NULL COMMENT 'quantidade no estoque',
  `image` INT(11) NULL COMMENT 'Imagem do produto',
  `slideshow` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_img` (`image` ASC),
  INDEX `fk_products_slideshow` (`slideshow` ASC),
  INDEX `fk_products_category` (`category` ASC),
  CONSTRAINT `fk_products_img`
    FOREIGN KEY (`image`)
    REFERENCES `mvcosmos`.`mv_midias_image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_slideshow`
    FOREIGN KEY (`slideshow`)
    REFERENCES `mvcosmos`.`mv_publications_slideshows` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_category`
    FOREIGN KEY (`category`)
    REFERENCES `mvcosmos`.`mv_products_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_doc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_doc` (
  `id` INT(11) NOT NULL,
  `user` INT(11) NOT NULL,
  `type` VARCHAR(45) NULL,
  `local` VARCHAR(255) NULL COMMENT 'Local do documento ou link para acesso',
  `content` TEXT NULL COMMENT 'Texto do documento',
  PRIMARY KEY (`id`),
  INDEX `fk_doc_user` (`user` ASC),
  CONSTRAINT `fk_doc_user`
    FOREIGN KEY (`user`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_chat_messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_chat_messages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sender` INT(11) NOT NULL,
  `receiver` INT(11) NOT NULL,
  `text` TEXT NOT NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `status` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_chatmessages_sender` (`sender` ASC),
  INDEX `fk_chatmessages_receiver` (`receiver` ASC),
  CONSTRAINT `fk_chatmessages_sender`
    FOREIGN KEY (`sender`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chatmessages_receiver`
    FOREIGN KEY (`receiver`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_chat_care`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_chat_care` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sender` INT(11) NOT NULL,
  `receiver_email` VARCHAR(255) NOT NULL,
  `receiver_name` VARCHAR(255) NULL,
  `text` TEXT NOT NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `status` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_chatcare_sender` (`sender` ASC),
  CONSTRAINT `fk_chatcare_sender`
    FOREIGN KEY (`sender`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_helpdesk_problems_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_helpdesk_problems_categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `priority` INT(5) NOT NULL COMMENT 'Nível de prioridade',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_helpdesk_solutions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_helpdesk_solutions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` TEXT NOT NULL,
  `href` TEXT NULL COMMENT 'Link externo para consulta ou exclarecimento',
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_helpdesk_problems`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_helpdesk_problems` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `category` INT(11) NOT NULL,
  `description` TEXT NOT NULL,
  `solution` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_helpdeskproblems_category` (`category` ASC),
  INDEX `fk_helpdeskproblems_solution` (`solution` ASC),
  CONSTRAINT `fk_helpdeskproblems_category`
    FOREIGN KEY (`category`)
    REFERENCES `mvcosmos`.`mv_helpdesk_problems_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_helpdeskproblems_solution`
    FOREIGN KEY (`solution`)
    REFERENCES `mvcosmos`.`mv_helpdesk_solutions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_helpdesk`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_helpdesk` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `client` INT(11) NOT NULL,
  `problem` INT(11) NOT NULL,
  `observation` TEXT NOT NULL COMMENT 'Detalhes do problema',
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_helpdesk_client` (`client` ASC),
  INDEX `fk_helpdesk_problem` (`problem` ASC),
  CONSTRAINT `fk_helpdesk_client`
    FOREIGN KEY (`client`)
    REFERENCES `mvcosmos`.`mv_client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_helpdesk_problem`
    FOREIGN KEY (`problem`)
    REFERENCES `mvcosmos`.`mv_helpdesk_problems` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_helpdesk_contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_helpdesk_contact` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` INT(1) NOT NULL COMMENT '1:email; 2:phone;',
  `value` VARCHAR(255) NOT NULL,
  `contact` VARCHAR(255) NULL COMMENT 'Pessoa ou departamento para contato',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvcosmos`.`mv_helpdesk_chat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvcosmos`.`mv_helpdesk_chat` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `client` INT(11) NOT NULL,
  `consultant` INT(11) NOT NULL,
  `text` TEXT NOT NULL,
  `createdAt` DATETIME NULL,
  `updatedAt` DATETIME NULL,
  `status` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_helpdeskchat_client` (`client` ASC),
  INDEX `fk_helpdeskchat_consultant` (`consultant` ASC),
  CONSTRAINT `fk_helpdeskchat_client`
    FOREIGN KEY (`client`)
    REFERENCES `mvcosmos`.`mv_client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_helpdeskchat_consultant`
    FOREIGN KEY (`consultant`)
    REFERENCES `mvcosmos`.`mv_users_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
