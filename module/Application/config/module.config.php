<?php
namespace Application;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
  'router' => array(
    'routes' => array(
      'home' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/',
          'defaults' => array(
            'controller' => 'Application\Controller\Index',
            'action' => 'index',
          ),
        ),
      ),
      // The following is a route to simplify getting started creating
      // new controllers and actions without needing to create a new
      // module. Simply drop new controllers in, and you can access them
      // using the path /application/:controller/:action
      'application' => array(
        'type' => 'Literal',
        'options' => array(
          'route' => '/app',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'default' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:controller[/:action]]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
          'angular' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '-ng/[:action]',
              'constraints' => array(
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(
                'controller' => 'Angular',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'controllers' => array(
    'invokables' => array(
      'Application\Controller\Index' => 'Application\Controller\IndexController',
      'Application\Controller\Angular' => 'Application\Controller\AngularController',
      'Application\Controller\Status' => 'Application\Controller\StatusController',
    ),
  ),
  'view_manager' => array(
    'template_map' => array(
      'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
      'layout/clean' => __DIR__ . '/../view/layout/clean.phtml',
      'error/404' => __DIR__ . '/../view/error/404.phtml',
      'error/index' => __DIR__ . '/../view/error/index.phtml',
      'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
      'application/angular/welcome' => __DIR__ . '/../view/application/angular/welcome.phtml',
    ),
    'template_path_stack' => array(
      __DIR__ . '/../view',
    ),
  ),
  // Placeholder for console routes
  'console' => array(
    'router' => array(
      'routes' => array(
        'show-status' => [
          'options' => array(
            'route' => 'app status',
            'defaults' => array(
              'controller' => 'Application\Controller\Status',
              'action' => 'index'
            )
          )
        ]
      ),
    ),
  ),
);
