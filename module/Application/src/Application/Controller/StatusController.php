<?php
/**
 * Created by PhpStorm.
 * User: Marcus
 * Date: 11/06/2015
 * Time: 11:41
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class StatusController extends AbstractActionController{

	public function indexAction()
	{
		return new JsonModel(array('data'=>'Seja Bem Vindo'));
	}

}
