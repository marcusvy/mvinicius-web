<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AngularController extends AbstractActionController
{

    public function welcomeAction()
    {
        return new ViewModel();
    }

    public function sidenavAction()
    {
        return new ViewModel();
    }

}

