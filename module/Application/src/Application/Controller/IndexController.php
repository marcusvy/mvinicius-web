<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    public function indexAction()
    {
        return new ViewModel();
    }

    public function welcomeAction()
    {
        $data = [
              'title' => 'Olá!',
              'message' => 'Estamos pronto para começar'
            ];
            return new JsonModel($data);
    }

}

