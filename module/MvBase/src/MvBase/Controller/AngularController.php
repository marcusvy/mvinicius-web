<?php
namespace MvBase\Controller;

use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class AngularController
  extends AbstractActionController
{
  public function onDispatch(MvcEvent $e)
  {
    parent::onDispatch($e);
    $controller = $e->getTarget();
    $controller->layout('layout/clean.phtml');
    $controller->getResponse()->getHeaders()
      ->addHeaderLine('Content-type', 'text/html');
  }
}
