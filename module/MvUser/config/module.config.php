<?php
namespace MvUser;

return array(
  'router' => array(
    'routes' => array(
      'mv-user' => array(
        'type' => 'Literal',
        'options' => array(
          'route' => '/mv/user',
          'defaults' => array(
            '__NAMESPACE__' => 'MvUser\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'default' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:controller[/:action]]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
          'angular' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '-ng/[:action]',
              'constraints' => array(
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(
                'controller' => 'Angular',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'controllers' => array(
    'invokables' => array(
      'MvUser\Controller\Index' => 'MvUser\Controller\IndexController',
      'MvUser\Controller\Angular' => 'MvUser\Controller\AngularController',
      'MvUser\Controller\User' => 'MvUser\Controller\UserController',
    ),
  ),
  'view_manager' => array(
    'template_map' => array(
      'mv-user/index/index' => __DIR__ . '/../view/mv-user/index/index.phtml',
    ),
    'template_path_stack' => array(
      __DIR__ . '/../view',
    ),
  ),
);
