<?php

namespace MvUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class UserController
  extends AbstractActionController
{

  public function listAction()
  {
    $lista = [
      ["id" => 1, "name" => "Marcus Vinícius", "email" => "marcus@gmail.com", "ativo" => true],
      ["id" => 2, "name" => "Deise Lima", "email" => "deise@gmail.com", "ativo" => true],
      ["id" => 3, "name" => "Dart Vader", "email" => "dartinho@bol.com", "ativo" => false],
      ["id" => 4, "name" => "Marcus Vinícius", "email" => "marcus@gmail.com", "ativo" => true],
      ["id" => 5, "name" => "Deise Lima", "email" => "deise@gmail.com", "ativo" => true],
      ["id" => 6, "name" => "Dart Vader", "email" => "dartinho@bol.com", "ativo" => false],
      ["id" => 7, "name" => "Marcus Vinícius", "email" => "marcus@gmail.com", "ativo" => true],
      ["id" => 8, "name" => "Deise Lima", "email" => "deise@gmail.com", "ativo" => true],
      ["id" => 9, "name" => "Dart Vader", "email" => "dartinho@bol.com", "ativo" => false],
    ];
    return new JsonModel([
      'collection' => $lista
    ]);
  }


}

