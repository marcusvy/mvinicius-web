<?php

namespace MvUser\Controller;

use MvBase\Controller\AngularController as NgController;
use Zend\View\Model\ViewModel;

class AngularController
  extends NgController
{

  public function userListAction()
  {
    return new ViewModel();
  }

  public function userFormAction()
  {
    return new ViewModel();
  }
}

